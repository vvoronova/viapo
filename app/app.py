import requests
from flask import Flask
from fp.fp import FreeProxy

app = Flask(__name__)

random_wiki_page_url = 'https://en.wikipedia.org/w/api.php?format=json&action=query&generator=random&grnnamespace=0&prop=revisions%7Cimages&rvprop=content&grnlimit=10'


@app.route('/test')
def hello_world():
    response = None

    proxy = FreeProxy().get()
    session = requests.Session()
    session.proxies = dict(http=proxy, https=proxy)

    try:
        response = session.get(random_wiki_page_url)
    except (requests.exceptions.ProxyError, requests.exceptions.SSLError):
       pass

    if not response:
        return "something went awry with proxy or wiki, but nobody cares", 200

    i = 0
    for _ in response.text:
        i += 1

    return f"Success, {i}", 200


@app.route('/')
def health_check():
    return 'healthy', 200


if __name__ == "__main__":
    app.run(host='0.0.0.0')
